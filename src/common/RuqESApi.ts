import {extractPostInfo, getRuqqusWindow, PostInfo} from './common';
import {getPosts} from './selectors';

interface RuqESApi {
  getPosts: () => PostInfo[];
}

export const setupGlobalApi = async () => {
  const api: RuqESApi = {
    getPosts: () => getPosts().toArray().map((rawEl) => extractPostInfo($(rawEl))),
  };
  (getRuqqusWindow() as any).RuqES = api;
};
