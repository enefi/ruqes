import * as chai from 'chai';
import {describe, it} from 'mocha';
import {clone} from 'ramda';

import {swapIndices} from './swapIndices';

const {expect} = chai;
chai.should();

describe('swapIndices', () => {
  it('ignores invalid indices', () => {
    const orig = ['a', 'b', 'c'];
    const input = clone(orig);
    expect(swapIndices(-1)(0)(input)).to.eql(orig);
    expect(swapIndices(0)(-1)(input)).to.eql(orig);
    expect(swapIndices(3)(0)(input)).to.eql(orig);
    expect(swapIndices(0)(3)(input)).to.eql(orig);
  });

  it('swaps indices', () => {
    const orig = ['a', 'b', 'c'];
    const input = clone(orig);
    expect(swapIndices(0)(1)(input)).to.eql(['b', 'a', 'c']);
    expect(swapIndices(1)(0)(input)).to.eql(['b', 'a', 'c']);
    expect(swapIndices(2)(0)(input)).to.eql(['c', 'b', 'a']);
  });
});
