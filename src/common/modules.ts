import {Config, readConfig} from 'common/config';
import {RuqESModule} from 'common/RuqESModule';
import {debugLog} from './common';

const modules: RuqESModule[] = [];
let somePostsWereFullyHidden = false;

export interface ModuleSetupArgs {
  silent?: boolean;
}

export const registerClassModule = (cls: RuqESModule) => {
  cls.registerSomePostsFullyHiddenHandler(() => somePostsWereFullyHidden = true);
  modules.push(cls);
}

const readConfigOrDefault = async (): Promise<Config> => await readConfig();

export const handleFirstSetupOfModules = async () => {
  debugLog('modules', 'handleFirstSetupOfModules');
  const cfg = await readConfigOrDefault();
  modules.forEach(m => {
    m.setup({}, cfg);
    m.markFirstSetupRunFinished();
  });
};

export const handleModulesAfterContentChange = async () => {
  debugLog('modules', 'handleModulesAfterContentChange');
  const cfg = await readConfigOrDefault();
  modules.forEach(m =>
    m.onContentChange({silent: true}, cfg)
  );
  if (somePostsWereFullyHidden) {
    await handleOnSomePostsFullyHidden();
    somePostsWereFullyHidden = false;
  }
};

const handleOnSomePostsFullyHidden = async () => {
  debugLog('modules', 'handleOnSomePostsFullyHidden');
  const cfg = await readConfigOrDefault();
  modules.forEach(m => m.onSomePostsFullyHidden(cfg))
};
