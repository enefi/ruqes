import {getLeftSidebarEl, getMyGuildsInSidebarEl, getRightSidebarEl} from 'common/selectors';
import {
  hideScrollbarCls,
  sidebarButtonCls,
  sidebarButtonIconCls,
  sidebarClosedCls,
  sidebarFavoritesCls,
  sidebarFavoritesDeleteItemCls,
  sidebarFavoritesDownItemCls,
  sidebarFavoritesEditButtonCls,
  sidebarFavoritesEditingCls,
  sidebarFavoritesGuildBtnCls,
  sidebarFavoritesUpItemCls,
  sidebarIndependentScrollCls,
  sidebarSmallGuildImageCls,
} from 'common/styles';
import {
  $c,
  debugLog,
  genJsAnchor,
  genNavigateToRuqESUrl,
  genRuqESUrl,
  getCurrentSort,
  getGuildImageOnGuildPage,
  getGuildNameFromPathname,
  isNumber,
  isOnGuildPage,
  isOnPathOrSubpageGen,
  printError,
  setClass,
} from 'common/common';
import {Config, FavoriteGuild, FavoriteItem, readConfig, writeConfig} from 'common/config';
import {RuqESModule} from 'common/RuqESModule';
import {ModuleSetupArgs} from 'common/modules';
import {renderFavoritesInSidebar} from './renderFavoritesInSidebar';
import {swapIndices} from 'common/swapIndices';
import savedPfPPng from 'assets/saved_pfp.png';

export class SidebarModule extends RuqESModule {
  private toggleSidebar() {
    const btn = $c(sidebarButtonCls);
    btn.toggleClass('opened');
    const opened = btn.hasClass('opened');
    setClass(getRightSidebarEl(), sidebarClosedCls, !opened);
  };

  private genClickHandlerOfMoveButtons = (offset: 1 | -1) => (evt: JQuery.ClickEvent) => {
    const el = $(evt.target);
    const idxARaw = el.closest('li').data('index');
    if (idxARaw === undefined) {
      printError('genClickHandlerOfMoveButtons', 'failed to get index of current fav');
      return;
    }
    const idxA = parseInt(idxARaw);
    if (!isNumber(idxA)) {
      printError('genClickHandlerOfMoveButtons', 'invalid index', idxA);
      return;
    }
    this.swapFavorites(idxA, idxA + offset);
  };

  private setupFavSidebar(cfg: Config): void {
    const oldEl = $c(sidebarFavoritesCls);
    const wasEditing = oldEl.hasClass(sidebarFavoritesEditingCls);
    oldEl.remove();
    const bar = getLeftSidebarEl();
    const body = bar.find('.sidebar-section > .body');
    const el = renderFavoritesInSidebar(cfg, cfg.sidebar.favorites);
    debugLog('setupFavorites', {bar, cfg, body});
    if (wasEditing) { el.addClass(sidebarFavoritesEditingCls); }
    el.find(`.${sidebarFavoritesEditButtonCls}`).click((evt: JQuery.ClickEvent) => {
      el.toggleClass(sidebarFavoritesEditingCls);
    });
    el.find(`.${sidebarFavoritesDeleteItemCls}`).click((evt: JQuery.ClickEvent) => {
      const el = $(evt.target);
      const idx = el.closest('li').data('index');
      if (idx !== undefined) { this.removeFromFavorites(idx); }
    });
    el.find(`.${sidebarFavoritesUpItemCls}`).click(this.genClickHandlerOfMoveButtons(-1));
    el.find(`.${sidebarFavoritesDownItemCls}`).click(this.genClickHandlerOfMoveButtons(1));
    body.children().first().after(el);
  }

  private async addGuildToFavorites(name: string): Promise<void> {
    const newItem: FavoriteGuild = {
      tag: 'guild',
      guildName: name,
      openTarget: '_self',
      label: null,
      sort: getCurrentSort(),
      image: getGuildImageOnGuildPage(),
    };
    await this.addToFavorites(newItem);
  }

  private async addToFavorites(item: FavoriteItem): Promise<void> {
    const cfg = await readConfig();
    cfg.sidebar.favorites.push(item);
    await writeConfig(cfg);
    this.setupFavButton(cfg);
    this.setupFavSidebar(cfg);
  }

  private async removeFromFavorites(index: number): Promise<void> {
    const cfg = await readConfig();
    cfg.sidebar.favorites = cfg.sidebar.favorites.filter((_, idx) => idx !== index);
    await writeConfig(cfg);
    this.setupFavButton(cfg);
    this.setupFavSidebar(cfg);
  }

  private async removeGuildFromFavorites(name: string) {
    const cfg = await readConfig();
    const idx = cfg.sidebar.favorites.findIndex(x => x.tag === 'guild' && x.guildName === name);
    if (idx !== undefined) { await this.removeFromFavorites(idx); }
  }

  private async swapFavorites(idxA: number, idxB: number): Promise<void> {
    const cfg = await readConfig();
    const maxIdx = cfg.sidebar.favorites.length - 1;
    if (idxA < 0 || idxA > maxIdx || idxB < 0 || idxB > maxIdx) { return; }
    cfg.sidebar.favorites = swapIndices(idxA)(idxB)(cfg.sidebar.favorites);
    await writeConfig(cfg);
    this.setupFavSidebar(cfg);
  }

  private setupFavButton(cfg: Config): void {
    if (!isOnGuildPage()) { return; }
    $c(sidebarFavoritesGuildBtnCls).remove();
    const gName = getGuildNameFromPathname();
    const isInFavorites = Boolean(cfg.sidebar.favorites.find(x => x.tag === 'guild' && x.guildName === gName));
    const iconCls = isInFavorites ? 'fas fa-star' : 'far fa-star';
    const iconEl = $('<i>').addClass(iconCls).addClass('mr-1');
    const target = $('#main-content-col').children().first().next().find('.col');
    const btnTextEl = $('<span>').text(isInFavorites ? 'Remove from favorites' : 'Add to favorites');
    const el = genJsAnchor().addClass(sidebarFavoritesGuildBtnCls)
      .append(iconEl)
      .append(btnTextEl);
    el.click(() => {
      if (!isInFavorites) {
        this.addGuildToFavorites(gName);
      } else {
        this.removeGuildFromFavorites(gName);
      }
    });
    target.append(el);
  }

  private setupFavorites(cfg: Config): void {
    this.setupFavSidebar(cfg);
    this.setupFavButton(cfg);
  }

  private isOnHelpPage = isOnPathOrSubpageGen('help');

  private setupSavedContentLinkInFeeds(): void {
    const path = 'saved';

    const linkUnfoldedEl = $(`
<li class="guild-recommendations-item">
    <a href="${genRuqESUrl(path)}">
        <div class="d-flex">
            <div>
                <img src="data:image/png;base64,${btoa(savedPfPPng)}" class="profile-pic profile-pic-30">
            </div>
            <div class="my-auto ml-2">
                <div class="text-black font-weight-normal">Saved</div>
            </div>
        </div>
    </a>
</li>
    `.trim());
    linkUnfoldedEl.find('a').click(genNavigateToRuqESUrl(path));

    const linkFoldedEl = $(`
<li class="guild-recommendations-item">
    <a href="${genRuqESUrl(path)}">
        <img src="data:image/png;base64,${btoa(savedPfPPng)}" class="profile-pic profile-pic-30 transition-square">
    </a>
</li>
    `.trim());
    linkFoldedEl.find('a').click(genNavigateToRuqESUrl(path));

    const feedsSections = getLeftSidebarEl().find('.body > .mb-4').first().find('ul');
    feedsSections.first().append(linkUnfoldedEl);
    feedsSections.eq(1).append(linkFoldedEl);
  }

  private setupRightButton(sidebarCfg: Config['sidebar']) {
    const iconEl = $('<span>').addClass(sidebarButtonIconCls);
    const btnEl = $('<a>').addClass('btn opened').addClass(sidebarButtonCls).html(iconEl).click(this.toggleSidebar);
    getRightSidebarEl().prepend(btnEl);
    debugLog('[SidebarModule] setupRightButton', {
      'isOnGuildPage()': isOnGuildPage(),
      'sidebarCfg.autoHideRightButNotOnGuildPage': sidebarCfg.autoHideRightButNotOnGuildPage,
    });
    if (sidebarCfg.autoHideRight) {
      if (!sidebarCfg.autoHideRightButNotOnGuildPage || !isOnGuildPage()) {
        this.toggleSidebar();
      }
    }
  }

  async setup(args: ModuleSetupArgs, cfg: Config) {
    const sidebarCfg = cfg.sidebar;
    debugLog('setupSidebar', sidebarCfg);
    if (sidebarCfg.rightButton) {
      this.setupRightButton(sidebarCfg);
    }
    if (sidebarCfg.independentScroll && !this.isOnHelpPage()) {
      getRightSidebarEl().wrapInner($('<div>').addClass(sidebarIndependentScrollCls).addClass(hideScrollbarCls));
    }
    if (sidebarCfg.favoritesEnabled) {
      this.setupFavorites(cfg);
    }
    if (sidebarCfg.smallGuildImages) {
      getLeftSidebarEl().find(`.body > .mb-4:not(.${sidebarFavoritesCls}), .body > .pl-0`).addClass(sidebarSmallGuildImageCls);
    }
    if (sidebarCfg.savedContentLinkInFeeds && cfg.save.enabled) {
      this.setupSavedContentLinkInFeeds();
    }
    if (sidebarCfg.hideContentOfMyGuilds) {
      getMyGuildsInSidebarEl().find('ul').hide();
    }
  }
}
