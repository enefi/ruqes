import {Config, FavoriteItem} from 'common/config';
import {
  nonAggressiveCls,
  sidebarFavoritesCls,
  sidebarFavoritesDeleteItemCls,
  sidebarFavoritesDownItemCls,
  sidebarFavoritesEditButtonCls,
  sidebarFavoritesEditItemCls,
  sidebarFavoritesEmptyNoticeCls,
  sidebarFavoritesItemButtonCls,
  sidebarFavoritesShowOnlyInEditingCls,
  sidebarFavoritesSmallImageCls,
  sidebarFavoritesUpItemCls,
} from 'common/styles';
import {createRuqesMark} from 'common/common';
import {isEmpty} from 'ramda';

const getLinkFromItem = (x: FavoriteItem): string => {
  switch (x.tag) {
    case 'link':
      return x.url;
    case 'guild':
      const args = x.sort ? `?sort=${x.sort}` : '';
      return `/+${x.guildName}${args}`;
  }
};

const getLabelFromItem = (x: FavoriteItem): string => {
  if (x.label) { return x.label; }
  switch (x.tag) {
    case 'link':
      return x.url;
    case 'guild':
      return `+${x.guildName}`;
  }
};

const defaultGuildImage = '/assets/images/guilds/default-guild-icon.png';

const getImageFromItem = (x: FavoriteItem): string => {
  if (x.image) { return x.image; }
  switch (x.tag) {
    case 'link':
      return '/assets/images/icons/default_thumb_text.png'; // TODO?
    case 'guild':
      return defaultGuildImage;
  }
};

const renderBigItem = (x: FavoriteItem, i: number, allItems: FavoriteItem[]) => {
  const isLast = i === allItems.length - 1;
  const isFirst = i === 0;
  return `
  <li class="guild-recommendations-item d-flex justify-content-between w-100" data-index="${i}">
      <a
          href="${getLinkFromItem(x)}"
          target="${x.openTarget}"
          title="${getLabelFromItem(x)}"
          class="flex-grow-1 text-truncate"
      >
          <div class="d-flex">
              <div>
                  <img src="${getImageFromItem(x)}" class="profile-pic profile-pic-30 mr-2">
              </div>
              <div class="my-auto">
                  <div class="text-black font-weight-normal">${getLabelFromItem(x)}</div>
              </div>
          </div>
      </a>
      <div
          class="disabled ${sidebarFavoritesShowOnlyInEditingCls} ${sidebarFavoritesItemButtonCls} ${sidebarFavoritesEditItemCls}"
      >
          <i class="fas fa-feather-alt"></i>
      </div>
  
      <div
          class="${sidebarFavoritesShowOnlyInEditingCls} ${sidebarFavoritesItemButtonCls} ${sidebarFavoritesUpItemCls} ${isFirst ? 'disabled' : ''}"
            title="Move up"
        >
          <i class="fas fa-angle-up"></i>
      </div>
      <div
          class="${sidebarFavoritesShowOnlyInEditingCls} ${sidebarFavoritesItemButtonCls} ${sidebarFavoritesDownItemCls} ${isLast ? 'disabled' : ''}"
            title="Move down"
      >
          <i class="fas fa-angle-down"></i>
      </div>
  
      <div
          class="${sidebarFavoritesShowOnlyInEditingCls} ${sidebarFavoritesItemButtonCls} ${sidebarFavoritesDeleteItemCls}"
          title="Remove from favorites"
      >
          <i class="fas fa-skull"></i>
      </div>
  </li>
    `;
};

const renderSmallItem = (x: FavoriteItem, i: number): string => `
<li class="guild-recommendations-item" data-index="${i}">
    <a href="${getLinkFromItem(x)}" target="${x.openTarget}" title="${getLabelFromItem(x)}">
        <img src="${getImageFromItem(x)}" class="profile-pic profile-pic-30 transition-square">
    </a>
</li>
`;

export const renderFavoritesInSidebar = (cfg: Config, items: FavoriteItem[]): JQuery => {
  const html = `<div class="mb-4 ${sidebarFavoritesCls} ${cfg.sidebar.favoritesSmallImages ? sidebarFavoritesSmallImageCls : ''}">
    <div class="sidebar-collapsed-hidden">
        <div class="d-flex justify-content-between align-items-center mb-3">
            <div class="text-small font-weight-bold text-muted text-uppercase" style="letter-spacing: 0.025rem;">
                Favorites
                ${createRuqesMark().addClass(nonAggressiveCls).addClass('ml-2').prop('title', 'RuqES')[0].outerHTML}
            </div>
            <span
                class="px-0 ${nonAggressiveCls} ${sidebarFavoritesEditButtonCls}"
                title="Edit favorites"
            >
                <i class="fas fa-cog"></i> 
            </span>
        </div>
    </div>
    <div class="sidebar-collapsed-visible" title="Favorites (RuqES)">
        <i class="fas fa-star text-muted mb-3" style="font-size: 1rem;"></i>
    </div>
    <ul class="no-bullets guild-recommendations-list pl-0 sidebar-collapsed-hidden">
        ${items.map(renderBigItem).join('\n')}
    </ul>

    <ul class="no-bullets guild-recommendations-list pl-0 sidebar-collapsed-visible">
        ${items.map(renderSmallItem).join('\n')}
    </ul>
    
    <span class="${sidebarFavoritesEmptyNoticeCls} ${isEmpty(items) ? 'd-flex' : 'd-none'} p-3 flex-column justify-content-center align-items-center">
        <i class="fad fa-star mb-2"></i>
        <div class="text-center">
            No favorites, yet.<br>
            Use <i class="far fa-star"></i> to add some.
        </div>
    </span>
</div>
  `;
  const res = $(html);
  return res;
};
