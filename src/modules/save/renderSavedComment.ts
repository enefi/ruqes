import {CommentSave} from 'common/config';
import {encodeHtmlEntitiesDeeplyInStrings} from 'common/common';

export const renderSavedComment = (x: CommentSave): JQuery => {
  const html = `<!-- ${JSON.stringify(encodeHtmlEntitiesDeeplyInStrings(x), null, 4)} -->
<div id="comment-${x.id}" class="comment rounded">
<span
        class="comment-collapse d-md-block d-none"
        onclick="collapse_comment('${x.id}')"
></span>
    <div class="comment-body">
        <div id="comment-${x.id}-only">
            <div class="user-info"><a href="/@${x.author}" class="user-name ">${x.author}</a>
                <!-- <span class="time-stamp"> · 20 minutes ago TODO</span> -->
                <span
                        class="comment-collapse d-md-none"
                        onclick="collapse_comment('${x.id}')"
                ></span>
            </div>
            <div id="comment-text-${x.id}" class="comment-text mb-0">
                ${x.text}
            </div>
            <div id="comment-${x.id}-actions" class="comment-actions">
                <ul class="list-inline text-right text-md-left">
                    <li class="list-inline-item text-muted">
                        <a href="${x.link}"
                           rel="nofollow">
                            <i class="fas fa-link"></i>Permalink
                        </a>
                    </li>
                    <li class="list-inline-item text-muted d-none d-md-inline-block">
                        <a href="javascript:void(0);"
                           role="button"
                           class="copy-link"
                           data-clipboard-text="${x.link}">
                            <i class="fas fa-copy"></i>Copy link
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
`;
  const el = $(html);
  return el;
};
