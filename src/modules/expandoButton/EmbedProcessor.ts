import {path} from 'ramda';
import {ExpandoButtonModule} from './ExpandoButtonModule';
import {PostInfo, printError} from 'common/common';
import {Config} from 'common/config';

export interface LinkProcessorArgs {
  link: string;
  box: JQuery;
  cfg: Config;
  fromExpandDesktopImageCall?: {
    image: string,
    link: string
  }
  postEl: JQuery;
  postInfo: PostInfo;
}

export abstract class EmbedProcessor {
  protected genRegex(): RegExp | null { return null; }

  public urlMatches(link: string): boolean {
    const regex = this.genRegex();
    if (regex === null) { throw new Error(`Missing regex generator`); }
    return regex.test(link);
  }

  public async process(args: LinkProcessorArgs): Promise<boolean> {
    if (!this.urlMatches(args.link)) {
      return false;
    } else {
      return this.processMatchingLink(args);
    }
  }

  protected abstract async processMatchingLink(args: LinkProcessorArgs): Promise<boolean>;

  protected matchAllAndGetCapturedPart(link: string): string | null {
    const matches = [...link.matchAll(this.genRegex())];
    const res: string | undefined = path([0, 1], matches) || path([0, 2], matches);
    return res ?? null;
  }

  protected handleResponseError(box: JQuery, err: GM.Response<unknown>): void {
    const clsName = this.constructor.name;
    ExpandoButtonModule.writeErrorToExpandButtonBox(box, `${clsName}: Server responded with error #${err.status}.`);
    printError('[Embed]', clsName, err);
  }
}
