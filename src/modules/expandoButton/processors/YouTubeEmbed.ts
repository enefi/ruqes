import {EmbedProcessor, LinkProcessorArgs} from 'modules/expandoButton/EmbedProcessor';
import {ExpandoButtonModule} from 'modules/expandoButton/ExpandoButtonModule';

class YouTubeEmbed extends EmbedProcessor {
  protected genRegex(): RegExp | null {
    return /^https:\/\/(?:(?:www|m)\.)?youtube\.com\/watch\/?\?v=([\w-]+).*$|^https:\/\/youtu\.be\/([\w-]+)$/g;
  }

  protected async processMatchingLink({link, box, cfg}: LinkProcessorArgs): Promise<boolean> {
    const id = this.matchAllAndGetCapturedPart(link);
    if (!id) {
      console.log('failed to get YouTube video id', link);
      return false;
    }
    const el = $(`<div class="handleYouTubeVideo embed-responsive embed-responsive-16by9"><iframe width="560" height="315" src="https://www.youtube.com/embed/${id}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>`);
    ExpandoButtonModule.insertWrappedElementIntoBox(box, el);
    return true;
  }
}

export const youTubeEmbed = new YouTubeEmbed();
