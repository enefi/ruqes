import {EmbedProcessor, LinkProcessorArgs} from 'modules/expandoButton/EmbedProcessor';
import {ExpandoButtonModule} from 'modules/expandoButton/ExpandoButtonModule';
import {printError, xhrP} from 'common/common';

class ImgurBrokenEmbed extends EmbedProcessor {
  private static brokenImgurRegexGen = () => /^https:\/\/(?:m\.)?imgur\.com\/.*?([a-zA-Z0-9]+)$/gm;
  private static isImgurBroken = (x: string) =>
    ImgurBrokenEmbed.brokenImgurRegexGen().test(x) && !x.includes('gallery') && !x.includes('/a/');

  urlMatches(link: string): boolean {
    return ImgurBrokenEmbed.isImgurBroken(link);
  }

  protected async processMatchingLink({link, box, cfg}: LinkProcessorArgs): Promise<boolean> {
    try {
      const resp = await xhrP({
        method: 'GET',
        url: link,
      });
      const parsed = $(resp.response);
      const imgContainer = parsed.find('.post-image-container');
      const imgId = imgContainer.prop('id');
      const src = `https://i.imgur.com/${imgId}.png`;
      ExpandoButtonModule.insertImageIntoBox(box, src, cfg);
      return true;
    } catch (err) {
      printError('ImgurBrokenEmbed', 'failed to get or parse imgur document', err);
      return false;
    }
  }
}

export const imgurBrokenEmbed = new ImgurBrokenEmbed();
