import {EmbedProcessor, LinkProcessorArgs} from 'modules/expandoButton/EmbedProcessor';
import {ExpandoButtonModule} from 'modules/expandoButton/ExpandoButtonModule';

class RuqqusPostEmbed extends EmbedProcessor {
  protected genRegex(): RegExp | null {
    return /^https:\/\/(?:ruqqus\.com\/\+\w+\/post|ruqq\.us)\/([\w]+)(?:\/.*)?$/g;
  }

  protected async processMatchingLink({link, box, cfg, postInfo}: LinkProcessorArgs): Promise<boolean> {
    if (postInfo.textPost) { return false; }
    const id = this.matchAllAndGetCapturedPart(link);
    if (!id) {
      ExpandoButtonModule.writeErrorToExpandButtonBox(box, `Failed to extract id of a post from: "${link}"`);
      return true;
    }
    const el = $(`
<div class="embed-responsive embed-responsive-16by9">
    <iframe src="https://ruqqus.com/embed/post/${id}" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" frameborder="0"></iframe>
</div>
    `);
    ExpandoButtonModule.insertWrappedElementIntoBox(box, el);
    return true;
  }
}

export const ruqqusPostEmbed = new RuqqusPostEmbed();
