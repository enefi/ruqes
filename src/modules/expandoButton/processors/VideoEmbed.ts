import {EmbedProcessor, LinkProcessorArgs} from 'modules/expandoButton/EmbedProcessor';
import {ExpandoButtonModule} from 'modules/expandoButton/ExpandoButtonModule';

class VideoEmbed extends EmbedProcessor {
  protected genRegex(): RegExp | null {
    return /^https?:\/\/.*\.(?:mp4|webm|ogv)$/g;
  }

  protected async processMatchingLink({link, box}: LinkProcessorArgs): Promise<boolean> {
    const el = $(`
<div class="embed-responsive embed-responsive-16by9">
    <video controls>
    <source src="${link}"> 
        Sorry, your browser doesn't support embedded videos.
    </video>
</div>`);
    ExpandoButtonModule.insertWrappedElementIntoBox(box, el);
    return true;
  }
}

export const videoEmbed = new VideoEmbed();
