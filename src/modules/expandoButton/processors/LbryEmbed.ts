import {EmbedProcessor, LinkProcessorArgs} from 'modules/expandoButton/EmbedProcessor';
import {ExpandoButtonModule} from '../ExpandoButtonModule';
import {debugLog, printError, xhrP} from 'common/common';

class LbryEmbed extends EmbedProcessor {
  protected genRegex(): RegExp | null {
    return /^https:\/\/lbry.tv\/@[\w:]+\/[\w\-:]+$/;
  }

  protected async processMatchingLink({link, box, cfg}: LinkProcessorArgs): Promise<boolean> {
    try {
      const resp = await xhrP({
        method: 'GET',
        url: link,
      });
      debugLog('handleLbryLink', resp);
      const parsed = $('<div>').append($(resp.response));
      const videoUrl = parsed.find('meta[property="og:video"]').prop('content');
      const el = $(`<div class="handleLbryLink embed-responsive embed-responsive-16by9"><iframe src="${videoUrl}" allowfullscreen></iframe></div>`);
      ExpandoButtonModule.insertWrappedElementIntoBox(box, el);
      return true;
    } catch (err) {
      printError('LbryEmbed', err);
      return false;
    }
  }
}

export const lbryEmbed = new LbryEmbed();
