import {EmbedProcessor, LinkProcessorArgs} from 'modules/expandoButton/EmbedProcessor';
import {ExpandoButtonModule} from 'modules/expandoButton/ExpandoButtonModule';
import {debugLog, decodeHtmlEntitiesAggressively, printError, xhrP} from 'common/common';
import {boxMetaEmbedCls, boxMetaEmbedUrlCls} from 'common/styles';

class MetaEmbed extends EmbedProcessor {
  protected genRegex(): RegExp | null {
    return /^https?:\/\/.*$/;
  }

  protected async processMatchingLink({link, box, cfg}: LinkProcessorArgs): Promise<boolean> {
    try {
      const resp = await xhrP({
        method: 'GET',
        url: link,
      });
      debugLog('MetaEmbed', resp);
      const parsed = $('<div>').append($(resp.response));
      const imageUrl = parsed.find('meta[property="og:image"]').prop('content');
      const title = parsed.find('meta[property="og:title"]').prop('content');
      const description = parsed.find('meta[property="og:description"]').prop('content');
      const el = $('<div>').addClass(boxMetaEmbedCls).addClass('mb-2 px-2 pb-2 d-flex flex-column position-relative');
      // el.css({maxWidth: (box.width() - 10) + 'px'});
      const success = Boolean(imageUrl || description);
      debugLog('MetaEmbed', {imageUrl, title, description, success});
      el.append($('<div>').addClass('text-truncate').addClass(boxMetaEmbedUrlCls).text(link).prop('title', link));
      el.append($('<div>').addClass('mb-4 mt-2'));
      if (title) {
        const linkIcon =
          $('<i>').addClass('fas fa-external-link-alt text-small ml-2').css({transform: 'translate(0, -4px)'});
        const linkEl = $('<a>')
          .prop('target', '_blank').prop('href', link)
          .text(decodeHtmlEntitiesAggressively(title))
          .append(linkIcon)
        ;
        el.append($('<h3>').append(linkEl).addClass('d-flex mx-1 mt-0'));
      }
      if (description) {
        el.append($('<div>').text(decodeHtmlEntitiesAggressively(description)).addClass('mx-1 d-flex'));
      }
      if (imageUrl) {
        const imgWrapper = $('<div>').addClass('w-100 mt-2');
        imgWrapper.append(ExpandoButtonModule.genImageForBox(box, imageUrl, cfg, 'metaCard'));
        el.append(imgWrapper);
      }
      if (success) {
        ExpandoButtonModule.insertWrappedElementIntoBox(box, el);
      }
      return success;
    } catch (err) {
      printError('MetaEmbed', err);
      return false;
    }
  }
}

export const metaEmbed = new MetaEmbed();
