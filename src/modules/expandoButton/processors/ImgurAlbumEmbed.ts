import {drop} from 'ramda';

import {EmbedProcessor, LinkProcessorArgs} from 'modules/expandoButton/EmbedProcessor';
import {ExpandoButtonModule} from 'modules/expandoButton/ExpandoButtonModule';

class ImgurAlbumEmbed extends EmbedProcessor {
  private static readonly imgurAlbumPrefix = 'https://imgur.com/a/';

  private static isImgurAlbum = (x: string) => x.startsWith(ImgurAlbumEmbed.imgurAlbumPrefix);

  urlMatches(link: string): boolean {
    return ImgurAlbumEmbed.isImgurAlbum(link);
  }

  protected async processMatchingLink({link, box, cfg}: LinkProcessorArgs): Promise<boolean> {
    const albumId = drop(ImgurAlbumEmbed.imgurAlbumPrefix.length, link);
    const el = $(`
<blockquote class="imgur-embed-pub" lang="en" data-id="a/${albumId}">
    <a href="//imgur.com/a/${albumId}">View album ${albumId} on Imgur</a>
</blockquote>
<script async src="//s.imgur.com/min/embed.js" charset="utf-8"></script>
`);
    ExpandoButtonModule.insertWrappedElementIntoBox(box, el);
    return true;
  }
}

export const imgurAlbumEmbed = new ImgurAlbumEmbed();

