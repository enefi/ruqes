import {EmbedProcessor, LinkProcessorArgs} from 'modules/expandoButton/EmbedProcessor';
import {ExpandoButtonModule} from 'modules/expandoButton/ExpandoButtonModule';
import {debugLog, printError, xhrP} from 'common/common';

class TwitterEmbed extends EmbedProcessor {
  protected genRegex(): RegExp | null {
    return /^https:\/\/(?:mobile\.)?twitter\.com\/([\w0-9]+)\/status\/(\d+).*$/gm;
  }

  protected async processMatchingLink({link, box}: LinkProcessorArgs): Promise<boolean> {
    try {
      const resp = await xhrP({
        method: 'GET',
        url: `https://publish.twitter.com/oembed?url=${encodeURIComponent(link)}`,
      });
      const data = JSON.parse(resp.response);
      debugLog('handleTwitterLink', data);
      const parsed = $(data.html);
      // TODO: option to remove script
      const el = $('<div>').css({margin: '-10px 0'}).html(parsed);
      ExpandoButtonModule.insertWrappedElementIntoBox(box, el);
    } catch (err) {
      this.handleResponseError(box, err);
    }
    return true;
  }
}

export const twitterEmbed = new TwitterEmbed();
