import {EmbedProcessor, LinkProcessorArgs} from 'modules/expandoButton/EmbedProcessor';
import {ExpandoButtonModule} from 'modules/expandoButton/ExpandoButtonModule';

class TwitchClipEmbed extends EmbedProcessor {
  protected genRegex(): RegExp | null {
    return /^https:\/\/clips\.twitch\.tv\/(\w+).*$/g;
  }

  protected async processMatchingLink({link, box}: LinkProcessorArgs): Promise<boolean> {
    const id = this.matchAllAndGetCapturedPart(link);
    const el = $(`
<div class="embed-responsive embed-responsive-16by9">
    <iframe src="https://clips.twitch.tv/embed?clip=${id}&parent=ruqqus.com" frameborder="0" allowfullscreen="true" scrolling="no" height="378" width="620"></iframe>
</div>`);
    ExpandoButtonModule.insertWrappedElementIntoBox(box, el);
    return true;
  }
}

export const twitchClipEmbed = new TwitchClipEmbed();
