import {prop} from 'ramda';
import debounce from 'lodash.debounce';

import {Config} from 'common/config';
import {debugLog, preventDefaults, xhr, $i, createRuqesMark, genJsAnchor, $c} from 'common/common';
import logoSvg from 'assets/logo.svg';
import {
  improvedTableCls,
  loadTitleButtonCls,
  loadTitleButtonLoadingCls,
  nonAggressiveCls,
  postPreviewButtonCls,
  postPreviewCls,
} from 'common/styles';
import {getPostTitleInPostCreation, getPostUrlInPostCreation} from 'common/selectors';
import Event = JQuery.Event;
import {ModuleSetupArgs} from 'common/modules';
import {RuqESModule} from 'common/RuqESModule';
import {renderMarkdown} from './renderMarkdown';
import {ImageUploadButton} from '../external/ImageUploadButton';

export class CreatePostModule extends RuqESModule {
  private onLoadTitleButtonClick = (el: JQuery) => (evt: Event) => {
    debugLog('onLoadTitleButtonClick', el, evt);
    if (el.hasClass(loadTitleButtonLoadingCls)) {
      debugLog('onLoadTitleButtonClick - ignoring click, loading already in progress');
      return;
    }
    const url = String(getPostUrlInPostCreation().val());
    if (!url || (!url.startsWith('https://') && !url.startsWith('http://'))) {
      debugLog('onLoadTitleButtonClick - ignoring click, url doesn\'t look valid', url);
      return;
    }
    el.prop('disabled', true).addClass(loadTitleButtonLoadingCls);
    const enable = () => el.prop('disabled', false).removeClass(loadTitleButtonLoadingCls);
    xhr({
      method: 'GET',
      url,
      onload: resp => {
        if (resp.status !== 200) {
          return;
        }
        const parsed = $('<div>').append($(resp.response));
        const titleFromItemPropName = parsed.find('meta[itemprop=name]').prop('content');
        const titleFromTitleTag = parsed.find('title').first().text();
        debugLog('createPost - possible titles', {titleFromItemPropName, titleFromTitleTag});
        const title = titleFromItemPropName || titleFromTitleTag;
        if (!title) {
          debugLog('failed to get title', parsed);
        }
        getPostTitleInPostCreation().val(title);
        enable();
      },
      onerror: err => {
        debugLog('post loading failed', err);
        enable();
      },
    });
  };

  private appendElToUrlLabel = (el: JQuery) =>
    getPostUrlInPostCreation()
      .parent()
      .find('label')
      .append(el)
      .addClass('d-flex justify-content-between align-items-end');

  private setupLoadTitleButton = (cfg: Config['createPost']) => {
    if (!cfg?.loadTitleButton) {
      return;
    }
    if (window.location.pathname !== '/submit') {
      return;
    }

    const onlyForYouTube = false;

    const btn = $('<a>')
      .addClass('btn btn-secondary')
      .html(logoSvg)
      .append('Load title')
      .addClass(loadTitleButtonCls)
      .hide();
    if (!onlyForYouTube) {
      btn.show();
    }
    getPostUrlInPostCreation().change(evt => {
      const val = (evt.target as HTMLInputElement).value;
      const isYT = val.includes('youtube.com') || val.includes('youtu.be');
      if ((onlyForYouTube && isYT) || (!onlyForYouTube)) {
        btn.show();
      } else {
        btn.hide();
      }
    });
    this.appendElToUrlLabel(btn);
    btn.click(this.onLoadTitleButtonClick(btn));
  };

  private fillUrl = (url: string) => {
    getPostUrlInPostCreation().val(url).trigger('input');
  };

  private setupImgbbUploadButton = (cfg: Config) => {
    if (!cfg?.external?.imgbbKey) {
      return;
    }
    if (window.location.pathname !== '/submit') {
      return;
    }
    const btn = ImageUploadButton.genUploadButton('POST', cfg, url => this.fillUrl(url));
    this.appendElToUrlLabel(btn);
  };

  private setupPreview(cfg: Config) {
    if (!cfg.createPost.markdownPreview) { return; }
    const previewBox = $('<div>').addClass(postPreviewCls);
    if (cfg.post.improvedTableStyles) {
      previewBox.addClass(improvedTableCls);
    }
    const inputChangedHandler = debounce((evt: JQuery.ChangeEvent) => {
      const el = $(evt.target);
      previewBox.html(renderMarkdown(String(el.val())));
    }, 200);
    $i('post-text').on('input', inputChangedHandler);
    $('label[for=board]').before(previewBox);
    const toggleButton = genJsAnchor().addClass(postPreviewButtonCls).addClass('mx-1').html(
      $('<i>').addClass('fas fa-minus-circle').click((evt: JQuery.ClickEvent) => {
        previewBox.toggle();
        $(evt.target).toggleClass('fa-minus-circle fa-plus-circle');
      }),
    );
    const label = $('<label>').prop('href', 'javascript:void 0').text('Preview').addClass('d-block mt-1')
      .append(toggleButton).append(createRuqesMark().addClass(nonAggressiveCls));
    previewBox.before(label);
  }

  private setupOpenUrlButton(cfg: Config) {
    if (!cfg.createPost.openUrlButton) { return; }
    const onClick = () => {
      window.open(getPostUrlInPostCreation().val().toString(), '_blank');
    };
    const btn = $('<a>')
      .addClass('btn btn-secondary ml-2')
      .html(logoSvg)
      .append('Open')
      .addClass(loadTitleButtonCls)
      .prop('title', 'Open URL in new tab')
      .on('click', onClick);
    this.appendElToUrlLabel(btn);
  }

  async setup(args: ModuleSetupArgs, cfg: Config) {
    const creatPostCfg = cfg.createPost;
    if (!prop('silent', args || {})) {
      debugLog('setupCreatePost');
    }
    this.appendElToUrlLabel($('<i>').css({flexGrow: 10}));
    this.setupLoadTitleButton(creatPostCfg);
    this.setupImgbbUploadButton(cfg);
    this.setupPreview(cfg);
    this.setupOpenUrlButton(cfg);
  };
}
