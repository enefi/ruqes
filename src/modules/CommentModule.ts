import {prop, splitAt} from 'ramda';
import debounce from 'lodash.debounce';

import {
  $c,
  $i,
  createReactionGif,
  createReactionGifIco,
  createRuqesMark,
  createSmallCommentButton,
  debugLog,
  extractCommentInfo,
  genJsAnchor,
  genTextUpDownCounters,
  getBlockedComments,
  getCommentId,
  getValidComments,
  parseUpDownCounts,
  preventDefaults,
  printError,
  xhrP,
} from 'common/common';
import {
  clsPrefix,
  commentBiggerFoldButtonsCls,
  commentPreviewCls,
  commentUpDownVotesAsTextProcessedCls,
  improvedTableCls,
  commentPreviewAppliedCls,
  commentCollapseByRegexProcessedCls,
  commentImageUploadProcessedCls,
  commentReactionGifIcoProcessedCls,
  reactionGifPopUpCls,
  commentRevealBlockedProcessedCls,
  errorTextCls,
} from 'common/styles';
import {RuqESModule} from 'common/RuqESModule';
import {ModuleSetupArgs} from 'common/modules';
import {Config} from 'common/config';
import {getCommentReplyToPart, getComments, getReplyTextArea, getReplyToPost, isReplyToPost} from 'common/selectors';
import {renderMarkdown} from 'modules/createPost/renderMarkdown';
import {SaveModule} from './save/SaveModule';
import {ImageUploadButton} from './external/ImageUploadButton';

const genCommentId = (x: string) => `${clsPrefix}${x}`;

const processedCommentBoxMarkCls = genCommentId('processed-comment-box');

const textAreaIdDataName = 'textareaId';

interface ReactionGifInfo {
  name: string;
  url: string;
}

export const reactionGifs: ReactionGifInfo[] = [
  {
    name: "clapping",
    url: "https://i.ibb.co/sbkkygZ/clapping.gif",
  },
  {
    name: "laughing",
    url: "https://i.ibb.co/DYn0Gb8/laughing.gif",
  },
  {
    name: "lifting",
    url: "https://i.ibb.co/wy0H7gG/lifting.gif",
  },
  {
    name: "sad",
    url: "https://i.ibb.co/TvNKmDF/sad.gif",
  },
  {
    name: "angry",
    url: "https://i.ibb.co/r48rMF3/scowling.gif",
  },
  {
    name: "smug",
    url: "https://i.ibb.co/TqMz92L/smug.gif",
  },
  {
    name: "shocked",
    url: "https://i.ibb.co/J2XYxnC/surprised.gif",
  },
  {
    name: "weak",
    url: "https://i.ibb.co/H43XwjT/weak.gif",
  },
  {
    name: "shrug",
    url: "https://i.ibb.co/PY4Nf8s/what.gif",
  },
  {
    name: "giving crown",
    url: "https://i.ibb.co/VvKsWs4/giving-crown.gif",
  },
];

const popupFadeDuration = 200;
const previewDebounceTime = 200;

export class CommentModule extends RuqESModule {
  setupCtrlEnterToSend() {
    $('.comment-box')
      .filter((_, el) => !$(el).hasClass(processedCommentBoxMarkCls))
      .keydown(evt => {
        if (evt.key === 'Enter' && (evt.ctrlKey || evt.metaKey)) {
          debugLog('setupCtrlEnterToSend', 'CTRL+Enter detected', evt);
          const btn = $(evt.target).parent().find('a:contains(Comment)');
          if (btn.length > 1) {
            printError('setupCtrlEnterToSend', 'found multiple send buttons', btn);
          } else if (btn.length === 0) {
            printError('setupCtrlEnterToSend', 'no send button found', btn);
          } else {
            debugLog('setupCtrlEnterToSend', 'clicking on send button', btn);
            btn.click();
          }
        }
      })
      .addClass(processedCommentBoxMarkCls);
  };

  private setupBiggerFoldButtons() {
    $('body').addClass(commentBiggerFoldButtonsCls);
  }


  private setupUpDownVotesAsText() {
    getComments()
      .filter((_, rawEl) => !$(rawEl).hasClass(commentUpDownVotesAsTextProcessedCls))
      .each((_, rawEl) => {
        const el = $(rawEl);
        el.addClass(commentUpDownVotesAsTextProcessedCls);
        const pointsEl = el.find('.points');
        const [upCount, downCount] = parseUpDownCounts(pointsEl.data('originalTitle'));
        const counters = genTextUpDownCounters(upCount, downCount);
        el.find('.user-info .time-stamp').after(counters);
      })
    ;
  }

  private setupOneCommentPreview(cfg: Config, textArea: JQuery, insertPreviewBox: (_: JQuery) => void) {
    if (textArea.hasClass(commentPreviewAppliedCls)) {
      return;
    }
    textArea.addClass(commentPreviewAppliedCls);
    const previewBox = $('<div>').addClass(commentPreviewCls);
    if (cfg.post.improvedTableStyles) {
      previewBox.addClass(improvedTableCls);
    }
    const inputChangedHandler = debounce((evt: JQuery.ChangeEvent) => {
      const el = $(evt.target);
      previewBox.html(renderMarkdown(String(el.val())));
    }, previewDebounceTime);
    textArea.on('input', inputChangedHandler);
    insertPreviewBox(previewBox);
  }

  private setupPreview(cfg: Config) {
    const replyToPostEl = getReplyToPost();
    if (replyToPostEl?.length !== 1) {
      debugLog('CommentModule', 'setupPreview', 'failed to locate reply to post element', replyToPostEl);
    } else {
      this.setupOneCommentPreview(cfg, $('textarea[name="body"]', replyToPostEl), previewBox => replyToPostEl.append(previewBox));
    }
    if (SaveModule.isOnSavedPage()) { return; }
    getValidComments().each((_, rawEl) => {
      const el = $(rawEl);
      const replyEl = getCommentReplyToPart(el);
      if (!replyEl) {
        printError('CommentModule', 'setupPreview', 'failed to locate reply element', {el, replyEl});
        return;
      }
      this.setupOneCommentPreview(cfg, $('textarea[name="body"]', replyEl), previewBox => replyEl.find('form').after(previewBox));
    });
  }

  private setupAutoCollapsingOfCommentsByRegex(cfg: Config) {
    debugLog('setupAutoCollapsingOfCommentsByRegex', cfg.comment);
    const strRegex = cfg.comment.autoCollapseCommentsRegex;
    if (!strRegex) { return; }
    let regex: RegExp;
    try {
      regex = new RegExp(strRegex, 'isu');
    } catch (e) {
      printError('setupAutoCollapsingOfCommentsByRegex', 'invalid regex', e);
      return;
    }
    getComments().filter((_, rawEl) => !$(rawEl).hasClass(commentCollapseByRegexProcessedCls)).each((_, rawEl) => {
      const el = $(rawEl);
      el.addClass(commentCollapseByRegexProcessedCls);
      const text = el.find('.comment-text').text();
      if (regex.test(text)) {
        el.parent().parent().addClass('collapsed');
      }
    });
  }

  private insertMarkdownImageToTextArea(textEl: JQuery<HTMLElement>, url: string) {
    this.insertTextToTextArea(textEl, `![](${url})`);
  }

  private insertTextToTextArea(textEl: JQuery<HTMLElement>, text: string) {
    const insertPos = textEl.prop('selectionStart') || 0;
    const oldText = String(textEl.val());
    const [bef, aft] = splitAt(insertPos, oldText);
    const newText = bef + text + aft;
    debugLog("insertTextToTextArea", {insertPos, oldText, bef, aft, text, newText, textEl});
    textEl.val(newText).trigger('change').trigger('input');
  }

  private setupImageUpload(cfg: Config) {
    debugLog('setupImageUpload', cfg.comment, cfg.external);
    const processComment = (_, rawEl) => {
      const el = $(rawEl);
      if (el.hasClass(commentImageUploadProcessedCls)) { return; }
      el.addClass(commentImageUploadProcessedCls);
      const replyEl = isReplyToPost(el) ? el : getCommentReplyToPart(el);
      if (!replyEl) {
        printError('CommentModule', 'setupImageUpload', 'failed to locate reply element', {el, replyEl});
        return;
      }
      const lastActionButton = replyEl.find('.comment-format label').last();
      const onSuccess = url => {
        debugLog('setupImageUpload', 'onSuccess', url, cfg);
        const textEl = getReplyTextArea(replyEl);
        if (!textEl.length) {
          printError('setupImageUpload', 'onSuccess', 'failed to get textEl');
          return;
        }
        this.insertMarkdownImageToTextArea(textEl, url);
      };
      const btnEl = ImageUploadButton.genUploadButton('COMMENT', cfg, onSuccess);
      lastActionButton.after(btnEl);
    };
    getValidComments().each(processComment);
    getReplyToPost()?.each(processComment);
  }

  private reactionGifCommentClicked(eventTarget: JQuery) {
    if (!eventTarget.closest(`.${reactionGifPopUpCls}`).length) {
      this.closeReactionGifPopUp();
    }
  }

  private getReactionGifPopUpEl(): JQuery {
    return $c(reactionGifPopUpCls);
  }

  private closeReactionGifPopUp() {
    this.getReactionGifPopUpEl().fadeOut(popupFadeDuration);
  }

  private openReactionGifPopUp(evt: JQuery.Event, btnOffset: JQuery.Coordinates) {
    if (!this.getReactionGifPopUpEl().length) { this.createReactionGifsPopUp(); }
    const popUp = this.getReactionGifPopUpEl();
    const popUpHeight = popUp.height();

    popUp.css({
      top: Math.round((btnOffset.top - popUpHeight - 10)) + 'px',
      left: Math.round(btnOffset.left) + 'px',
    });
    popUp.fadeIn(popupFadeDuration);
    preventDefaults(evt);
  }

  private createReactionGifsPopUp() {
    const body = $('body');
    const popUp = $('<div>').addClass(reactionGifPopUpCls);
    const gifs = reactionGifs.map(gif =>
      createReactionGif(gif.name, gif.url)
        .on('click', () => {
          const textareaId = this.getReactionGifPopUpEl().data(textAreaIdDataName);
          if (!textareaId) {
            printError('createReactionGifsPopUp', 'missing text area id');
            return;
          }
          const textArea = $i(textareaId);
          this.insertMarkdownImageToTextArea(textArea, gif.url);
          this.closeReactionGifPopUp();
        }),
    );
    body.append(popUp);
    popUp.prepend(gifs).hide();
    body.on('click', event => { this.reactionGifCommentClicked($(event.target)); });
  }

  private setupReactionGifs = () => {
    const processComment = (_, rawEl) => {
      const el = $(rawEl);
      if (el.hasClass(commentReactionGifIcoProcessedCls)) { return; }
      el.addClass(commentReactionGifIcoProcessedCls);
      const replyEl = isReplyToPost(el) ? el : getCommentReplyToPart(el);
      if (!replyEl) {
        printError('CommentModule', 'setupReactionGifs', 'failed to locate reply element', {el, replyEl});
        return;
      }
      const textEl = getReplyTextArea(replyEl);
      if (!textEl.length) {
        printError('setupReactionGifs', 'onSuccess', 'failed to get textEl');
        return;
      }
      const lastActionButton = replyEl.find('.comment-format label').last();
      const btn = createSmallCommentButton('');
      btn.append(createReactionGifIco());
      lastActionButton.after(btn);

      btn.on('click', (evt) => {
        const offset = btn.offset();
        const textareaId = textEl.prop('id');
        if (!textareaId) {
          printError('setupReactionGifs', 'button click handler', 'missing text area id');
          return;
        }
        this.openReactionGifPopUp(evt, offset); // may get created at this point
        this.getReactionGifPopUpEl().data(textAreaIdDataName, textareaId);
      });
    };
    getValidComments().each(processComment);
    getReplyToPost()?.each(processComment);
  };

  private genRevealButtonClickHandler = (el: JQuery) => async (evt: JQuery.ClickEvent) => {
    const btnEl = $(evt.target);
    btnEl.prop('disabled', true).addClass('disabled').text('⏳');
    const fail = (errorText: string, ...args) => {
      printError('genRevealButtonClickHandler', errorText, ...args);
      const errorEl = $('<div>')
        .addClass(errorTextCls)
        .append($('<span>').text(errorText))
      ;
      el.append(errorEl);
    };
    const commentId = getCommentId(el);
    if (!commentId) {
      fail('Failed to get comment id', el);
      return;
    }
    let resp: GM.Response<unknown>;
    try {
      resp = await xhrP({
        method: 'GET',
        url: `https://ruqqus.com/+x/post/x/x/${commentId}`,
        anonymous: true,
      });
    } catch (e) {
      fail('Request failed', el, e);
      return;
    }
    debugLog('genRevealButtonClickHandler', 'got response', resp);
    const parsed = $('<div>').append($(resp.response));
    const commentFromResp = parsed.find(`#comment-${commentId}-only`);
    if (!commentFromResp.length) {
      fail('Failed to extract comment', {el, resp, parsed});
      return;
    }
    const info = extractCommentInfo(commentFromResp);
    const barEl = $('<div>')
      .append(`@${info.author}`)
      .append(' · ')
      .append($('<a>').text('link').prop('href', resp.finalUrl).prop('target', '_blank'))
    ;
    const textEl = $('<div>').html(info.text);
    const boxEl = $('<div>').append(barEl).append(textEl);
    el.append(boxEl);
    btnEl.remove();
  };

  private setupRevealBlockedComment(cfg: Config) {
    const process = (_, rawEl) => {
      const el = $(rawEl);
      if (el.hasClass(commentRevealBlockedProcessedCls)) { return; }
      el.addClass(commentRevealBlockedProcessedCls);
      const btn = genJsAnchor().addClass('btn btn-secondary').text('Reveal').prepend(createRuqesMark()).on('click', this.genRevealButtonClickHandler(el));
      el.append(btn);
    };
    getBlockedComments().each(process);
  }

  async setup(args: ModuleSetupArgs, cfg: Config) {
    const commentCfg = cfg.comment;
    if (!prop('silent', args)) {
      debugLog('setupComment', commentCfg);
    }
    if (!this.firstSetupRunFinished) {
      if (commentCfg.ctrlEnterToSend) {
        setInterval(this.setupCtrlEnterToSend, 1000);
      }
      if (commentCfg.biggerFoldButtons) {
        this.setupBiggerFoldButtons();
      }
    }
    if (commentCfg.upDownVotesAsText) {
      this.setupUpDownVotesAsText();
    }
    if (commentCfg.preview) {
      this.setupPreview(cfg);
    }
    if (commentCfg.hideTipButton) {
      getComments().find('[data-target="#tipModal"]').parent().css('display', 'none');
    }
    this.setupAutoCollapsingOfCommentsByRegex(cfg);
    if (commentCfg.imageUpload && cfg.external.imgbbKey) {
      this.setupImageUpload(cfg);
    }
    if (commentCfg.reactionGifs) {
      this.setupReactionGifs();
    }
    if (commentCfg.revealBlocked) {
      this.setupRevealBlockedComment(cfg);
    }
  }

  async onContentChange(args: ModuleSetupArgs, cfg: Config) {
    await this.setup(args, cfg);
  }
}
