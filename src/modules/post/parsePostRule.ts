import {any, fromPairs, isEmpty, KeyValuePair, map, pipe, split} from 'ramda';

import {PostRule} from '../../common/config';

type ParseResult<T> = { tag: 'fail', error: string } | { tag: 'ok', rule: T }

export const parseHeaderWrapper = (tag: string) => (text: string): { innerHeader: string, rest: string } | null => {
  const re = new RegExp(`\\/\\/\\s*==${tag}==\\s*\\n([\\s\\S]*)\\/\\/\\s*==\\/${tag}==\n(?:([\\s\\S]*))?`)
  const res = text.match(re);
  if (res === null) {
    return null;
  }
  if (isEmpty(res.length)) {
    return null;
  }
  return {innerHeader: res[1].trim(), rest: res[2] || ''};
};

export const parseHeaderValues = (text: string): Record<string, string | undefined> | null => {
  const processed = pipe(
    split('\n'),
    map(x => {
      const m = x.match(/^\/\/\s+@(\w+)\s+(.+)$/)
      if (!m || !m[1] || !m[2]) {
        return null;
      }
      return [m[1], m[2]] as [string, string];
    })
  )(text);
  if (any(x => x === null)(processed)) {
    return null;
  }
  return fromPairs(processed);
};

export const parsePostRule = (text: string): ParseResult<PostRule> => {
  const innerHeaderParsed = parseHeaderWrapper('RuqES_PostRule')(text);
  if (innerHeaderParsed === null) {
    return {tag: 'fail', error: 'Failed to locate header'};
  }
  const headerValues = parseHeaderValues(innerHeaderParsed.innerHeader);
  if (headerValues === null) {
    return {tag: 'fail', error: 'Failed to parse header values'}
  }
  if (!headerValues.name) {
    return {tag: 'fail', error: 'Missing name header value'}
  }
  if (!/^\w+$/.test(headerValues.name)) {
    return {tag: 'fail', error: 'Invalid name'}
  }
  const rule: PostRule = {
    name: headerValues.name,
    body: text,
    enabled: true,
  }
  return {tag: 'ok', rule};
};
