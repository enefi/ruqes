import * as chai from 'chai';
import {describe, it} from 'mocha';
import {parseHeaderValues, parseHeaderWrapper, parsePostRule} from './parsePostRule';

const {expect} = chai;
chai.should();

describe('parseHeaderWrapper', () => {
  it('accepts empty tag pair', () => {
    const t = `
// ==Q==
// ==/Q==
`.trimStart();
    expect(parseHeaderWrapper('Q')(t)).to.eql({innerHeader: '', rest: ''});
  });
  it('returns one-line inner part', () => {
    const t = `
// ==Q==
// @name value
// ==/Q==
`.trimStart();
    expect(parseHeaderWrapper('Q')(t)).to.eql({innerHeader: '// @name value', rest: ''});
  });
  it('returns two-line inner part', () => {
    const t = `
// ==Q==
// @name value
// @x y
// ==/Q==
`.trimStart();
    expect(parseHeaderWrapper('Q')(t)).to.eql({innerHeader: '// @name value\n// @x y', rest: ''});
  });
});

describe('parseHeaderValues', () => {
  it('parses one value', () => {
    const t = `
// @name value
    `.trim();
    expect(parseHeaderValues(t)).to.eql({name: 'value'});
  });
  it('parses more values', () => {
    const t = `
// @name value
// @pepe hands !
    `.trim();
    expect(parseHeaderValues(t)).to.eql({name: 'value', pepe: 'hands !'});
  });
});

describe('parsePostRule', () => {
  it('rejects empty', () => {
    expect(parsePostRule('').tag).to.be.eql('fail');
  });

  it('parses nice header', () => {
    const t = `
// ==RuqES_PostRule==
// @name Soval
// ==/RuqES_PostRule==

return;
    `.trim();
    expect(parsePostRule(t)).to.be.eql({tag: 'ok', rule: {name: 'Soval', body: '// ==RuqES_PostRule==\n// @name Soval\n// ==/RuqES_PostRule==\n\nreturn;'}});
  });

  it('parses nice header, body without leading empty line', () => {
    const t = `
// ==RuqES_PostRule==
// @name Soval
// ==/RuqES_PostRule==
return;
    `.trim();
    expect(parsePostRule(t)).to.be.eql({tag: 'ok', rule: {name: 'Soval', body: '// ==RuqES_PostRule==\n// @name Soval\n// ==/RuqES_PostRule==\nreturn;'}});
  });

  it('rejects name with spaces', () => {
    const t = `
// ==RuqES_PostRule==
// @name Pepe Hands
// ==/RuqES_PostRule==

return;
    `.trim();
    expect(parsePostRule(t).tag).to.be.eql('fail');
  });
});
