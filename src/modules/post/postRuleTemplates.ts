export interface PostRuleTemplate {
  name: string;
  sourceCode: string;
  customizationRecommended: boolean;
}

const trimSourceCode = (x: string): string => x.trim();

export const postRuleTemplates: ReadonlyArray<PostRuleTemplate> = [
  {
    name: 'BlurNsfwThumbnails',
    sourceCode: `
// ==RuqES_PostRule==
// @name BlurNsfwThumbnails
// ==/RuqES_PostRule==
if (post.nsfw) return A.blurThumbnailAction();
    `,
    customizationRecommended: false,
  },
  {
    name: 'HideVotedOn',
    sourceCode: `
// ==RuqES_PostRule==
// @name HideVotedOn
// ==/RuqES_PostRule==
if (post.vote) return A.hideAction();
    `,
    customizationRecommended: false,
  },
  {
    name: 'SemiHideVotedOn',
    sourceCode: `
// ==RuqES_PostRule==
// @name SemiHideVotedOn
// ==/RuqES_PostRule==
if (post.vote) return A.semiHideAction();
    `,
    customizationRecommended: false,
  },
  {
    name: 'SemiHideFromSpecificGuilds',
    sourceCode: `
// ==RuqES_PostRule==
// @name SemiHideFromSpecificGuilds
// ==/RuqES_PostRule==

const guilds = [
    'OffensiveJokes',
    'LGBT',
];
// replace "semiHideAction" with "hideAction" if you want posts fully hidden
if (guilds.includes(post.guild)) return A.semiHideAction();
    `,
    customizationRecommended: true,
  },
  {
    name: 'HideLinkingToDomains',
    sourceCode: `
// ==RuqES_PostRule==
// @name HideLinkingToDomains
// ==/RuqES_PostRule==
const blocked = [
  'https://i.ruqqus.com',
  'https://youtu.be',
  'https://www.youtube.com',
];
if (blocked.some(function (x){ return (post.link || '').startsWith(x)}))
  return A.hideAction();
    `,
    customizationRecommended: true,
  },
].map(t => ({...t, sourceCode: trimSourceCode(t.sourceCode)}));
