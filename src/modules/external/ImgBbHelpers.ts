import {debugLog, xhr} from '../../common/common';
import {path} from 'ramda';
import {Config} from '../../common/config';

export interface UploadImageToImgBbArgs {
  cfg: Config;
  file: Blob;

  onError(resp: GM.Response<any>): void;

  onSuccess(url: string): void;
}

export const uploadImageToImgBb = (args: UploadImageToImgBbArgs) => {
  const {cfg, file, onError, onSuccess} = args;
  debugLog('uploadImageToImgBb', {cfg, file});
  const doRequest = (image: string) => {
    debugLog('doRequest');
    const fd = new FormData();
    fd.append('image', btoa(image));
    const data = fd;
    debugLog({data});
    xhr({
      method: 'POST',
      url: `https://api.imgbb.com/1/upload?key=${cfg?.external?.imgbbKey}`,
      data: data as any, // probably incorrect typings
      onload: resp => {
        if (resp.status !== 200) { return onError(resp); }
        debugLog('imgbb upload success', resp, resp.response);
        let json = null;
        try {json = JSON.parse(resp.response);} catch (e) { }
        if (json) {
          const url = String(path(['data', 'url'])(json));
          debugLog('parsed imgbb response, url is', url, '; whole json = ', json);
          onSuccess(url);
        } else {
          alert(`Failed to parse response from ImgBB:\n${resp.response}`);
          onError(resp);
        }
      },
      onerror: err => onError(err),
    });
  };

  const reader = new FileReader();
  reader.onload = (readEvt) => {
    doRequest(readEvt.target?.result as string);
  };
  reader.readAsBinaryString(file);
};
