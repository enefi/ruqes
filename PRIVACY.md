We do not store any data from a user on our servers.

Data stored in user's browser via userscript extension:

* settings
* saved posts
* favorite items (e.g. guilds)

You may view your data in the userscript extension you are using to run the userscript (if the extensions supports it). Other way is to export your settings as JSON from the settings dialog.

If a user enables (or keeps enabled) an expando button or embeds then a code from 3rd party can be used to facilitate the embedding process (e.g. Twitter, YouTube, BitChute, lbry, Imgur). These services have its own privacy policies, we encourage users to review them.
